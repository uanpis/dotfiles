syntax on

" plugins
call plug#begin('~/.vim/plugged')

    Plug 'preservim/nerdcommenter'
    Plug 'preservim/nerdtree'
    map <C-o> :NERDTreeToggle<CR>

    Plug 'valloric/youcompleteme'
    Plug 'jiangmiao/auto-pairs'
    Plug 'itchyny/lightline.vim'
    Plug 'tpope/vim-surround'

    " syntax
    Plug 'cespare/vim-toml'
    Plug 'dag/vim-fish'

    " colorschemes
    Plug 'sonph/onehalf', { 'rtp': 'vim' }
    Plug 'rakr/vim-one'
    Plug 'ayu-theme/ayu-vim'
    Plug 'chriskempson/base16-vim'

    " Nice things
    Plug 'Yggdroot/indentLine'
    Plug 'dhruvasagar/vim-table-mode'

call plug#end()

filetype plugin on
filetype indent on
let mapleader = ' '

" Allow saving of files as sudo
cmap w!! w !sudo tee > /dev/null %

" change table corners and header chars
let g:table_mode_corner_corner='+'
let g:table_mode_header_fillchar='='
"let g:table_mode_separator = '│'

" change indent marker char
let g:indentLine_char = '│'

" disable ycm annoying popups
let g:ycm_auto_hover=''
"nnoremap gd :YcmCompleter GoToDefinitionElseDeclaration

" remap split resizing to be better
nnoremap <c-H> <c-w>5<
nnoremap <c-J> <c-w>5+
nnoremap <c-K> <c-w>5-
nnoremap <c-L> <c-w>5>

" cursor and numbers
set cursorline
set nu rnu
set scrolloff=7
set signcolumn=yes
set colorcolumn=77

" enable wrap and disable skipping of 'fake' lines
set wrap
nnoremap j gj
nnoremap k gk

" show all search matches and search as user is typing
set showmatch
set incsearch
set nohlsearch
set ignorecase smartcase

" autofold at 10
set foldenable
set foldlevelstart=10
set foldmethod=indent

" set tab width and tab settings
set tabstop=4 softtabstop=4 shiftwidth=4
set smartindent
set expandtab
filetype indent on

" completion
set completeopt=menuone,noselect

" lilypond
autocmd filetype lilypond nmap <F1> :!lilypond %<CR>

" tex
autocmd filetype tex nmap <F1> :!texi2pdf %<CR>

" misc
set lazyredraw
set hidden
set ttimeoutlen=0
set updatetime=50
set wildmenu

" memory
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile

" colors
set termguicolors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
source ~/.vim/colorscheme.vim

" statusline
set laststatus=2
set noshowmode
let g:lightline = { 'colorscheme': 'hopscotch' }

" autocommands
fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

augroup UANPIS
    autocmd!
    autocmd BufWritePre * :call TrimWhitespace()

    autocmd!
    autocmd BufWinLeave * mkview
    autocmd BufWinEnter * silent! loadview
augroup END




