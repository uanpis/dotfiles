#
# ~/.bashrc
# If not running interactively, don't do anything
export PATH=$PATH:$HOME/bin:$HOME/.local/bin

[[ $- != *i* ]] && return

alias grep='grep --color=auto'
alias ls='ls --color=auto'
alias la='ls -la --color=auto'

PS1='\e[34m \W \e[0m'
