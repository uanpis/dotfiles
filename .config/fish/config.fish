fish_add_path $HOME/bin
fish_add_path $HOME/.cargo/bin
fish_add_path $HOME/.local/bin
fish_add_path $HOME/.local/share/gem/ruby/3.0.0/bin

alias config='/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME'

# Skip all the following if not running in a terminal
if not status --is-interactive
  exit 0
end

# Base16 Shell
eval sh /home/uanpis/.base16-manager/chriskempson/base16-shell/scripts/base16-hopscotch.sh

# Set fish_greeting to fortune message
function fish_greeting
end

# Show distro logo and stats
