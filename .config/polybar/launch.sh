#!/bin/bash

killall -q polybar

config=~/.config/polybar/config
for m in $(xrandr --query | grep " connected" | awk -F " " '{print $1}'); do
    for bar in system network desktops keys time; do
        MONITOR=$m polybar $bar --reload -c $config &
    done
done
