[colors]
    transparent     = #00ffffff
    background      = #80b9b5b8
    background-alt  = #80797379
    foreground      = #322931
    foreground-alt  = #80433b42
    red             = #dd464c
    green           = #8fc13e
    yellow          = #fdcc59
    blue            = #1290bf
    magenta         = #c85e7c
    cyan            = #149b93

[general]
    height = 30
    offset-y = 10

[fonts]
    roboto = Roboto-Regular:size=11;2
    unifont = Unifont:size=11;2
    awesome = FontAwesome:size=13;2
    remixicon = remixicon:size=14;4
    weather = Weather\ Icons:size=12;3
    material = Material\ Icons:size=13;3

[bar/system]
    monitor = ${env:MONITOR:}
    offset-x = 15
    offset-y = ${general.offset-y}
    width = 240
    height = ${general.height}
    wm-restack = bspwm
    override-redirect = true

    background = ${colors.background}
    foreground = ${colors.foreground}

    padding-left = 3

    font-0 = ${fonts.roboto}
    font-1 = ${fonts.remixicon}

    modules-left = powermenu space wlan space alsa space battery

[bar/network]
    monitor = ${env:MONITOR:}
    offset-x = 265
    offset-y = ${general.offset-y}
    width = 50%:-375
    height = ${general.height}
    wm-restack = bspwm
    override-redirect = true

    background = ${colors.background}
    foreground = ${colors.foreground}

    font-0 = ${fonts.roboto}
    font-1 = ${fonts.unifont}
    font-2 = ${fonts.awesome}
    font-3 = ${fonts.remixicon}

    modules-center = polygraph

[bar/desktops]
    monitor = ${env:MONITOR:}
    offset-x = 50%:-100
    offset-y = ${general.offset-y}
    width = 200
    height = ${general.height}
    wm-restack = bspwm
    override-redirect = true

    background = ${colors.background}
    foreground = ${colors.foreground}

    font-0 = ${fonts.roboto}
    font-1 = ${fonts.remixicon}

    modules-center = bspwm

[bar/keys]
    monitor = ${env:MONITOR:}
    offset-x = 100%:-436
    offset-y = ${general.offset-y}
    width = 61
    height = ${general.height}
    wm-restack = bspwm
    override-redirect = true

    background = ${colors.background}
    foreground = ${colors.foreground}

    font-0 = ${fonts.roboto}

    modules-left = keys

[bar/weather]
    monitor = ${env:MONITOR:}
    offset-x = 100%:-365
    offset-y = ${general.offset-y}
    width = 160
    fixed-center = false
    height = ${general.height}
    wm-restack = bspwm
    override-redirect = true

    background = ${colors.background}
    foreground = ${colors.foreground}

    font-0 = ${fonts.roboto}
    font-1 = ${fonts.weather}
    font-2 = ${fonts.material}

    modules-center = weather

[bar/time]
    monitor = ${env:MONITOR:}
    offset-x = 100%:-195
    offset-y = ${general.offset-y}
    width = 180
    fixed-center = false
    height = ${general.height}
    wm-restack = bspwm
    override-redirect = true

    background = ${colors.background}
    foreground = ${colors.foreground}

    padding-left = 3
    padding-right = 3

    font-0 = ${fonts.roboto}
    font-1 = ${fonts.remixicon}

    modules-right = time
    modules-left = date

[module/keys]
    type = internal/xkeyboard
    label-layout = ""

    label-indicator-padding = 0

    label-indicator-on-capslock = "%{B#dd464c}  C  %{B-}"
    label-indicator-off-capslock = "  C  "

    label-indicator-on-numlock = "%{B#1290bf}  N  %{B-}"
    label-indicator-off-numlock = "  N  "

[module/polygraph]
    type = custom/script
    exec = polygraph
    tail = true
    format = <label>
    label = %output%
    interval = 1

[module/weather]
    type = custom/script
    exec = busctl --user -j get-property io.ntfd /weather openweathermap.strings RenderedTemplate | jq -r .data
    interval = 30

[module/alsa]
    type = internal/alsa
    interval = 2

    format-volume = <ramp-volume> <label-volume>
    label-volume = %percentage%%
    label-muted = 

    ramp-volume-0 = ""
    ramp-volume-1 = ""

[module/powermenu]
    type = custom/menu

    label-open = 
    label-open-foreground = ${colors.red}
    label-close = "  "
    label-close-foreground = ${colors.red}
    label-close-background = ${colors.background-alt}

    menu-0-0 = " "
    menu-0-0-background = ${colors.background-alt}
    menu-0-0-exec = killall Xorg
    menu-0-1 = " "
    menu-0-1-background = ${colors.background-alt}
    menu-0-1-exec = systemctl reboot
    menu-0-2 = " "
    menu-0-2-background = ${colors.background-alt}
    menu-0-2-exec = systemctl poweroff

[module/battery]
    type = internal/battery
    battery = BAT0
    adapter = ADP0
    full-at = 100

    format-full-prefix = " "
    format-charging =  <label-charging>
    format-discharging = <ramp-capacity> <label-discharging>

    ramp-capacity-0 = 
    ramp-capacity-1 = 
    ramp-capacity-2 = 

[module/wlan]
    type = custom/script
    exec = bash $HOME/.config/polybar/wlan

    label = %output%
    interval = 5

    format-connected = <ramp-signal>
    format-disconnected = 

[module/date]
    type = internal/date
    interval = 1
    date = "%a %d %B"
    label = %date%

[module/time]
    type = internal/date
    interval = 1
    time = "%H:%M:%S"
    label = %time%

[module/space]
    type = custom/text
    content = "   "

[module/bspwm]
    ws-icon-0 = 1;
    ws-icon-1 = 2;
    ws-icon-2 = 3;
    ws-icon-3 = 4;
    ws-icon-4 = 5;

    ws-icon-5 = 6;
    ws-icon-6 = 7;
    ws-icon-7 = 8;
    ws-icon-8 = 9;
    ws-icon-9 = 0;

    type = internal/bspwm

    label-focused = %icon%
    label-focused-background = ${colors.background-alt}
    label-focused-padding = 4

    label-occupied = %icon%
    label-occupied-padding = 2

    label-urgent = %icon%!
    label-urgent-background = ${colors.alert}
    label-urgent-padding = 2

    label-empty = %icon%
    label-empty-foreground = ${colors.foreground-alt}
    label-empty-padding = 2
